#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 100

struct student{
    char firstname[MAX_SIZE];
    char subject[MAX_SIZE];
    int marks;
};

int main()
{
    int i;
    struct student st[10];
    printf("Enter details of the Students:\n");
    for(i=0;i<5;i++){
        printf("\nEnter First Name :");
        scanf("%s",&st[i].firstname);
        printf("Enter Subject :");
        scanf("%s",&st[i].subject);
        printf("Enter Marks :");
        scanf("%d",&st[i].marks);
    }
    printf("\nStudent Details :\n");
    for(i=0;i<5;i++){
        printf("\nFirst Name : %s\n",st[i].firstname);
        printf("Subject : %s\n",st[i].subject);
        printf("Marks : %d\n",st[i].marks);
    }
    return 0;
}
